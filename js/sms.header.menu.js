/**
 * sms.header.menu.js
 * http://roadsidemultimedia.com/
 * Author: Curtis Grant, Barak Llewellyn, Ryan P.C. McQuen
 */
    jQuery(function($) {
      $(window).ready(function() { 
          $('.rsnav').meanmenu();          
          var example = $('.rs-menu').superfish();     
          var navOffset = $('.fullheader').offset();
          var fullheaderHeight = $('.fullheader').height();
              $(window).scroll(function() {
                if($(document).scrollTop() > navOffset.top) {
                    $(".fullheader").addClass("fixed");
                    $(".mspacer").height(fullheaderHeight);
                }
                else {
                    $(".fullheader").removeClass("fixed");
                    $(".mspacer").height(0);
                    // this refreshes the entire header when the scrollbar
                    // returns to the top, eliminating that weird ghost menu
                    $("header").toggle().toggle();
                }
              });
      });
      $( window ).resize(function() {
        if ($(window).width() < 1080) {
          $(".fullheader").removeClass('fixed');
          $(".mspacer").height(0);
        }
      });
    });
